library(reshape2)
library(gplots)
library("RColorBrewer")
results = NULL
results = read.table("results.txt")
head(results)

m = acast(results, V2~V1, value.var = "V3")
# m[which(rownames(m) == 897),]
m[which(is.na(m))] = 0
m[which(m < 5)] = 0
m[which(m >= 7)] = 12
head(m)
# pdf(file = "heatmap.pdf")
# heatmap.2(head(m, n = 2000),dendrogram='none', Rowv=FALSE, Colv=FALSE,trace='none', labCol="", labRow="") # , col=yellowred(10))

#######
m = acast(results, V2~V1, value.var = "V3")
m[which(is.na(m))] = 0
m[which(m < 5)] = 0
m[which(m >= 5)] = 1
m

library(ggplot2)


pdf(file = "test.pdf", width = 100, height = 10)
par(mfrow=c(2,1))

sums = rowSums(m[seq(0, dim(m)[1], 1),])
sums = as.data.frame(as.matrix(sums))
sums$V2 = sums$V1
sums$V1 = rownames(sums)
# plot(sums, ylim = c(0,dim(m)[2]), type = "s")

sums$V1 = as.numeric(sums$V1)
sums$type = "persistence"

ggplot(sums, aes(V1,V2)) + geom_step() + #Ploting
  scale_y_continuous(name= "") +
  scale_x_continuous(name= "") +
  geom_ribbon( data = sums, aes( ymin = 0, ymax = V2 ),
               fill="tomato", alpha=0.5 )

coords = read.table("operon.txt") # ("Clostridium botulinum B str. Eklund 17B")
coords = coords[order(coords$V1),]
rownames(coords) = coords[,1]

coords$type = "operon"

ggplot(coords, aes(V1,V2)) + geom_step() + #Ploting
  scale_y_continuous(name= "") +
  scale_x_continuous(name= "") +
  geom_ribbon(data = coords, aes( ymin = 0, ymax = V2 ),
               fill="black", alpha=0.5) +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.ticks.y=element_blank(),
        axis.text.y = element_blank())

  dev.off()
  
combined = rbind(coords, sums)

p <- ggplot(data = combined, aes(x = V1, y = V2)) + 
  geom_step() +
  # geom_point(aes(color = "type")) + 
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.ticks.y=element_blank(),
        axis.text.y = element_blank())

p <- p + facet_grid(type ~ ., scales = "free_y")

library(grid)
library(gtable)
gt = ggplot_gtable(ggplot_build(p))
gt$widths[4] = 1*gt$widths[4]
grid.draw(gt)
gtable_show_layout(gt)

# plot(c(length(which(m[4450,] > 0)),
# length(which(m[3971,] > 0)),
# length(which(m[3972,] > 0)),
# length(which(m[3973,] > 0)),
# length(which(m[3974,] > 0)),
# length(which(m[3975,] > 0)),
# length(which(m[3976,] > 0)),
# length(which(m[3977,] > 0)),
# length(which(m[3978,] > 0)),
# length(which(m[3979,] > 0))))
# 
# m[3970,]
# which(rowSums(m) == 185)

# values = rowSums(m)
# table(values)
# values = values[which(values > 1)]
# hist(table(values), breaks = 200)



DF <- data.frame(date = as.Date(runif(100, 0, 800),origin="2005-01-01"), 
                 outcome = rbinom(100, 1, 0.1))
DF <- DF[order(DF$DateVariable),] #Sort by date
DF$x <- seq(length=nrow(DF)) #Add case numbers (in order, since sorted)
DF$y <- cumsum(DF$outcome)
library(ggplot2)
ggplot(DF, aes(x,y)) + geom_step() + #Ploting
  scale_y_continuous(name= "Number of failures") +
  scale_x_continuous(name= "Operations performed")



