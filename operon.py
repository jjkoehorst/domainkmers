def main():
	writer = open("operon.txt","w")
	numbers = []
	for line in open("Clostridium botulinum B str. Eklund 17B"):
		left, right = map(int, line.strip().split())
		numbers += list(range(left, right))
		# print(left - 1, 0, file=writer)
		# print(left, 10, file=writer)
		# print(right, 10, file=writer)
		# print(right + 1, 0, file=writer)
	numbers = set(numbers)
	reference = list(range(0, max(numbers)))
	for index, position in enumerate(reference):
		if position in numbers:
			reference[index] = 1
		else:
			reference[index] = 0

	count = 0
	for index, element in enumerate(reference): # [1:40000]):
		if index > 0:
			previous = reference[index - 1]
			current = reference[index]
			if previous == current:
				count = count + 1
			else:
				print(index-count, previous, file=writer)
				print(index, previous, file=writer)
				count = 0
	#print(index, count, previous)


if __name__ == '__main__':
	main()